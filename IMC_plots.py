
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
from PIL import Image
import pickle


#step_size = args[0] # step size between vertical lines
#band_width = args[1] # bandwith should be 1/2 step_size for full coverage of the point cloud

fig, ax1 = plt.subplots()

#ax1.set_title("Linear mask", fontsize=18)
ax1.set_xlabel("Distance X [m]", fontsize=14)
ax1.set_ylabel('Distance Y [m]', fontsize=14)
background = Image.open("data/clipped_orthophoto.tif")  #
im = ax1.imshow(background, extent=[0, 20, 0, 20], alpha=.9)
ax1
