import pandas as pd

from scipy.spatial.transform import Rotation
from PyEMD import EMD
from scipy.interpolate import griddata
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import matplotlib
import multiprocess as mp
import plots
import pickle
import smoothing

def rotator(args):
    """
    The point cloud is rotated, the points are pinned to the closest vertical line and the moving window is made over these lines.
    :param args:
    :return:
    field - point cloud data with updated z values
    slopes - steepest slopes from pc
    """
    step_size = args[0] # step size between vertical lines
    band_width = args[1] # bandwith should be 1/2 step_size for full coverage of the point cloud
    df = args[2] # input data x,y,z this is our working dataframe
    rot = args[3] # list of rotation angles
    rotation_z = []
    rotation_grad = []
    rotation_std =[]
    lookup = []
    #rotates the point cloud
    my_rotation = Rotation.from_euler('z', rot, degrees=True)               # define rotation angle e.g. -10 returns a rotation matrix
    cdf = df.copy(deep=True)  # make a copy of original data for rotating it
    cdf = cdf.rename(columns={"X":"rX", "Y":"rY"}, errors="raise")
    cdf[['rX','rY']] = my_rotation.apply(cdf.iloc[:, 0:3] )[:,:2]          # apply rotation using 3 first cols x, y and z then only return the x and y rotated values.
    list_x = list(np.arange(cdf['rX'].min(), cdf['rX'].max() + band_width, step_size))   # divide up the rotatied x axis where vertical lines will live

    for i, x in enumerate(list_x):         # iterate over the lines - enumerate creates an "index" var that counts from 0
        aline = cdf[(cdf['rX'] >= x-band_width/2) & (cdf['rX'] < x+band_width/2)]     # choose data close to the line
        aline  = aline.sort_values(by=['rY'])                    # sort selected data along y
        aline = aline.reset_index()  # reset the index (maybe not required)

        ############ EMD section #########################
        #todo: change the smoothing to test EMD
        #field, slopes = smoothing.EMD_smoothing(list_x, cdf)
        #field, slopes = smoothing.MW_smoothing(aline, rot)

        ############ Moving window ################
        # todo: Must change the moving window to a distance not numnber of pts, PC doesn't have a uniform density of points making a pt based MW not ideal
        window = 2000 # moving widow size slope
        window_roughness = 50
        if len(aline['lookup']) > window: # make sure that the corners of the point cloud dont make any super short lines.
            MW_z, MW_grad, MW_std = smoothing.MW_smoothing(aline, window, window_roughness) # here is the moving window calculation

        ########## merging the lines to DEM ##############
            rotation_z = rotation_z + MW_z.tolist() # this adds all the "aline" lines together merging the lines back into a full data set
            rotation_grad += MW_grad.tolist() # from a line worth of data to full data frame worth of data
            rotation_std += MW_std.tolist()
            lookup += MW_z.index.tolist()

        plot = False # swithc for aline plot
        if rot == 0 and plot is True:
            plots.aline_plot(aline, MW_z, rot)

    rotation = (lookup, rotation_z, rotation_grad, rotation_std)


    # this plots the points that are part of aline but didn't get calculated
    # this also plots the lines         
    error_plots = False
    if error_plots is True: # plots x_lines and points withplots no z calculated
       plots.error_plot(cdf, lookup,list_x)

    return rotation



path_data = 'data/'                      # read the point cloud
#filename  = 'data/Punktwolke_AOI_gross.csv'
#df = pd.read_csv(filename, names= ["X","Y","Z"], skiprows = 1)
filename =  "data/22-02-09.txt"
df = pd.read_csv(filename,sep='\s+', names= ["X","Y","Z"], skiprows = 1) # calls in file with space as delimiter, skips file name and gives it xyz to cols


minx = df["X"].min() # these are used to scale the axis when plotted and to scale the background photo
miny = df["Y"].min()
maxx = df["X"].max()
maxy = df["Y"].max()

#filename = 'synthetic-smooth'
"""filename = 'synthetic'
infile = open(filename,'rb')
df = pickle.load(infile)
infile.close()
#load in synthetic data set"""

lookup = np.arange(0,len(df["X"]) )
df["lookup"]  = lookup.tolist()
df.set_index("lookup")
width_X = df['X'].max()-df['X'].min()
width_Y = df['Y'].max()-df['Y'].min()
width_Z = df['Z'].max()-df['Z'].min()
width_X, width_Y, width_Z

# Definitions
step_size = 5
band_width = 5

columns_z = []
angles = [0, 15,30,45, 60,75, 90,105,120,135,150,165,180]
#angles = [0,45,90]

for rot in angles: #todo: this is where you multi-process
    df["z_{}".format(rot)] = np.nan
    df["slope_{}".format(rot)] = np.nan
    df["std_{}".format(rot)]= np.nan
    arg = [step_size, band_width, df, rot] # package inputs into a list (pool.map will only take one input hence the list)
    rotation_results = rotator(arg) # this is our rotation function, emd_results is a list containing 2 series.
    lookup = rotation_results[0]
    z_value = pd.Series(rotation_results[1], index=rotation_results[0])
    gradient = pd.Series(rotation_results[2], index=rotation_results[0])
    std = pd.Series(rotation_results[3], index=rotation_results[0])
    #df["z_{}".format(rot)] = np.where(['stream'] == 2, 10, 20)
    df["z_{}".format(rot)] = z_value #rotation_results[1]  # adds rotated data onto our working data frame.
    df["slope_{}".format(rot)] = gradient #rotation_results[2]
    df["std_{}".format(rot)] = std
    columns_z.append("z_{}".format(rot))



###### build DEM
max_slope_direction = df.loc[:, df.columns.str.startswith('slope')].idxmax(axis=1) # picks out the column where the maximum slope is
df['down_slope_std'] = 0
for a, k in enumerate(max_slope_direction): # todo this is super slow find out how to do this out of a loop
    df["down_slope_std"].iloc[a] = df["std_" + k[6:]].iloc[a] # strips slope_xx to make col name "std_xx"
max_slope_direction = max_slope_direction.str.split("_").str.get(1) # split string "slope_0" at _ and take number
df["max_slope_direction"] = max_slope_direction.astype(int) #(max_slope_direction)# converts max slope direction colum name into an int and saves it in our df
df["max_slope"] = df.loc[:, df.columns.str.startswith('slope')].max(axis=1) # picks out the value of maximum slopes
df["ave_z"] =  df.loc[:, df.columns.str.startswith('z_')].mean(axis=1) # average z of all rotations calculated
colors = df["down_slope_std"] # colors for the plot are the standard dev of moving window in the downhill direction.
#plots.three_D(X,Y,Z, colors)
#plots.three_D(X,Y,df["z_0"], colors)
#plots.two_D(X, Y, colors)
pkl = True # todo: pickle the df and the rotations for large datasets. Max/min and plotting can be post processing step
if pkl is True:
    filename = 'data/22-02-09_pickle.p'
    outfile = open(filename,'wb')
    pickle.dump(df, outfile)
    outfile.close()

plots.roughneess_terrain_plot(df,colors, maxx, minx, maxy, miny)

plt.show()



