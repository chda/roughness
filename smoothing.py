import pandas as pd
from PyEMD import EMD
import numpy as np


def MW_smoothing(aline, window, window_roughness):
    """
    This function needs to return a series for field and slopes.... that is it.
    :param list_x: the lines crossing the point clouds
    :param cdf: copy of the data frame with all xyz points and a unique ID
    :return:
    """
    # we should make sure that we do not need to reset the index here....
    # we want the lines y valuse in order over the moving window
    MW_z = aline["Z"].rolling(window, min_periods=1).mean()               #moving window
    MW_z.index = aline["lookup"]
    MW_std = aline["Z"].rolling(window_roughness, min_periods=1).std()
    MW_std.index = aline['lookup']
    MW_grad = pd.Series(np.gradient(MW_z), index=MW_z.index )# index should already be in order, slope calculated on the new z values.

    return MW_z, MW_grad, MW_std


def EMD_smoothing(list_x, cdf):
    lines = []
    slope_lines = []
    for i, x in enumerate(list_x):                                                                             # iterate over the lines - enumerate creates an "index" var that counts from 0
        aline = {}
        aline['df']  = cdf[(cdf['rX']>=x-band_width/2) & (cdf['rX']<x+band_width/2)]                           # choose data close to the line
        aline['df']  = aline['df'].sort_values(by=['rY'])                                                      # sort selected data along y
        #aline['df']  = aline['df'].reset_index()                                                               # reset the index (maybe not required)
        if len(aline['df'])>1:      # if there are points in the line
            aline['emd'] = EMD()                                                                               # define emd
            aline['IMF'] = aline['emd'].emd(aline['df']['Z'].values, aline['df']['rY'].values)                 # fit emd
            N = aline['IMF'].shape[0]+1
            for n, imf in enumerate(aline['IMF']): # iteration over IMFs
                value, grad, residuals         = smoothed_terrain(aline['IMF'], n-1, aline['df']['Z'].values)  # calculate terrain
                aline['df']['Z%d'%(n)]         = value                                                         # update dataframe with local calculated altitude
                aline['df']['gradZ%d'%(n)]     = grad                                                          # local gradient, slope inclination in the desired direction
                aline['df']['residuals%d'%(n)] = residuals                                                     # local residual
            aline['df'] = aline['df'].sort_index()
            #lines[i] = aline["df"]
            #list_points = aline["df"]['Z5']
            lines.append(aline["df"]['Z5']) # list of all z5 in unordered list index should match the points lookup of the raw df
            slope_lines.append(aline["df"]["gradZ%d"%(n)])
    field = pd.concat(lines)
    slopes = pd.concat(slope_lines)
    return field, slopes

def smoothed_terrain(IMF, d, obs):
    N = IMF.shape[0]
    if d >= N:
        return np.nan
    y = 0
    for i in range(N-1, d, -1):
        y += IMF[i]
    residuals = y-obs
    return y, np.gradient(y), residuals
