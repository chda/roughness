import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
from PIL import Image
import pickle

from mpl_toolkits.axes_grid1 import make_axes_locatable

def three_D(X,Y,Z, colors):
    fig = mpl.pyplot.figure()
    ax = mpl.pyplot.axes(projection='3d')
    # ax.contour3D(X, Y, Z5, 50, cmap='binary')
    tri = mtri.Triangulation(X, Y)
    ax.plot_trisurf(X, Y, Z, triangles=tri.triangles, cmap=plt.cm.Spectral, edgecolor='none');  # cmap='cubehelix',
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

def three_Diff(X, Y, Z, colors):
    fig = mpl.pyplot.figure()
    ax = mpl.pyplot.axes(projection='3d')
    # ax.contour3D(X, Y, Z5, 50, cmap='binary')
    tri = mtri.Triangulation(X, Y)
    ax.plot_trisurf(X, Y, colors, triangles=tri.triangles, cmap=plt.cm.Spectral, edgecolor='none');  # cmap='cubehelix',
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')


def two_D(X,Y, colors):
    fig = mpl.pyplot.figure()

    # colormap
    N = 20
    cmap = plt.get_cmap('jet', N)

    ax = mpl.pyplot.axes()
    ax.scatter(X,Y, c=colors, cmap=cmap )
     # color bar made with https://www.geeksforgeeks.org/matplotlib-pyplot-colorbar-function-in-python/ example 3

    # Normalizer
    norm = mpl.colors.Normalize(vmin=min(colors), vmax=max(colors))

    # creating ScalarMappable
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    plt.colorbar(sm, ticks=np.linspace(min(colors), max(colors), N))


def aline_plot(aline, MW_z, rot):
    fig = plt.figure()
    ax = plt.axes()
    ax.plot(aline["rY"], aline["Z"], color= "blue", label="projected PC")  # aline.index, c = aline.index)#, cmap='viridis')
    ax.plot(aline["rY"], MW_z, color="red", label="moving window")
    # plots.two_D(aline.index, aline["Z"], aline["rY"])
    plt.title("rotation {}".format(rot))
    plt.xlabel('distance along line')
    plt.ylabel("elevation")
    plt.legend()
    plt.show()


def error_plot(cdf, lookup,list_x):
    x_error = []
    y_error = []
    z_error = []
    for i in range(len(cdf.index)):
        if i not in lookup:
            # print(i)
            row = cdf.loc[[i]]
            x_error.append(float(row["rX"]))
            y_error.append(float(row["rY"]))
            z_error.append(float(row["Z"]))
    # x_error = np.asarray(x_error, y_error,z_error)

    fig = mpl.pyplot.figure()
    ax = mpl.pyplot.axes()
    for x in list_x:
        ax.axvline(x)
    ax.scatter(x_error, y_error, color="red")
    print(len(x_error))
    plt.show()

def roughneess_terrain_plot(df,colors, maxx, minx, maxy, miny, image):
    fig, ax1 = plt.subplots()
    Z = df['Z']
    X = df['X']
    Y = df['Y']
    N = 20
    cmap = plt.get_cmap('YlOrRd', N)
    U = -1 * np.cos(np.deg2rad(df["max_slope_direction"]))  # x direction of arrows
    V = 1 * np.sin(np.deg2rad(df["max_slope_direction"]))  # y direction of arrows
    # plt.quiver(X[:1000],Y[:1000],V[:1000],U[:1000], colors, cmap=cmap)
    n = 1000 # only plot every nth point so the plot is not saturated
    print("one")

    ### adapt color scale so low or high values dont saturate our color
    colors[colors< .5] = .5
    colors[colors > 2] = 2



    #scale_units and scale are needed to change arrow size...not sure how yet. 
    arrows = ax1.quiver(X[::n] - minx, Y[::n] - miny, V[::n], U[::n] ,colors[::n], scale_units= "xy", scale=1.5, cmap=cmap)
    print("two")
    ax1.set_title("Roughness along fall line", fontsize=18)
    ax1.set_xlabel("Distance X [m]", fontsize=14)
    ax1.set_ylabel('Distance Y [m]', fontsize=14)
    clb = plt.colorbar(arrows,ticks=np.linspace(0, .003, 7))

    clb.ax.set_title(' Roughness score [m]', fontsize=14, pad=-50, y=.45, rotation=90)
    #clb.set_clim(-1 , -10)
    # cbar = cmap.colorbar()
    # cbar.ax.set_title('Roughness (elevation - smoothed elevation) [m]')
    # ax1.axis('equal')
    # cp = ax1.contour([X, Y], Z, 10) # todo: add contour from smoothed df["ave_z"] over ortho photo.
    # ax1.clabel(cp, inline=True, fontsize=10)

    background = Image.open(image)  #
    # plt.imread("orthophoto.png")

    extentx = int(maxx - minx)
    extenty = int(maxy - miny)
    im = ax1.imshow(background, extent=[0, extentx, 0, extenty], alpha=.9)


"""
fig = mpl.figure(figsize=(7,7)); suptitle('Winkel '+str(winkel))
ax1 = mpl.subplot(321)                                                                                           # show terrain



ax1.tricontour(x, y, z, levels=14, linewidths=0.5, colors='k')
ax1.tricontourf(x, y, z, levels=14, cmap="RdBu_r")
ax1b = subplot(322, sharex=ax1, sharey=ax1)                                                                  # show data points used for lines
ax1b.tricontourf(x, y, z, levels=14, cmap="RdBu_r")
ax1b.scatter(x,y, c='k',s=.1)
mpl.show()
"""
if __name__ == '__main__': # run this code if you have pickled the data frame

    #df = pickle.load( open( "emd_pickle.p", "rb" ) )
    date ="21-10-27"
    df = pickle.load(open("data/" + date + "_pickle.p", "rb"))
    # photo names NoSnow_clipped_orthophoto.tif  2022-02-09_clipped_orthophoto.tif  2022-03-01_clipped_orthophoto.tif  2022-03-25_clipped_orthophoto.tif
    image = "data/20"+date+ "_clipped_orthophoto.tif" # the background photo
    colors = df["down_slope_std"]
    minx = df["X"].min()  # these are used to scale the axis when plotted and to scale the background photo
    miny = df["Y"].min()
    maxx = df["X"].max()
    maxy = df["Y"].max()
    print("one")
    roughneess_terrain_plot(df, colors, maxx, minx, maxy, miny, image)
    print("two")
    #plt.hist(colors, bins= [0,.01,.02,.05,.1,.5,1,2,5,10] )
    plt.savefig("figs_code_validation/" + date, format = "png", dpi = 400)
    plt.show()

